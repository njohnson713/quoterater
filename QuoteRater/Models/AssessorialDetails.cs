﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.Models
{
    public class AccessorialDetails
    {
        public int ShipAccessID { get; set; }
        public int ShipmentID { get; set; }
        public int AccessorialID { get; set; }
        public string AccessorialCode { get; set; }
        public decimal Charge { get; set; }
    }
}
