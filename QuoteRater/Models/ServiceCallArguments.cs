﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.Models
{
    public class ServiceCallArguments
    {
        public string UserID { get; set; }
        public string Password { get; set; }
        public string AccountNo { get; set; }
        public string OriginCity { get; set; }
        public string OriginState { get; set; }
        public string OriginZip { get; set; }
        //public string OriginCountry { get; set; }
        public string DestCity { get; set; }
        public string DestState { get; set; }
        public string DestZip { get; set; }
        public string DestCountry { get; set; }
        public string Country { get; set; }
        public int QuoteId { get; set; }
        public int SiteID { get; set; }
        public string ThirdAddress { get; set; }
        public string ThirdCity { get; set; }
        public string ThirdState { get; set;}
        public string ThirdZip { get; set; }
        public int Third { get; set; }
    }
}
