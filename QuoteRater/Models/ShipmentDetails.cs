﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.Models
{
    public class ShipmentDetails
    {
        public int ShipmentDetailID { get; set; }
        public int ShipmentID { get; set; }
        public int ParcelID { get; set; }
        public decimal Weight { get; set; }
        public string Class { get; set; }
        public string Pieces { get; set; }
        public string Description { get; set; }
        public decimal PackagingWeightPct { get; set; }
        public decimal PackagingWeight { get; set; }
        public Boolean TrueLTLRate { get; set; }
        public decimal Surchage { get; set; }
        public int QuoteID { get; set; }
        public string Rank { get; set; }
        public string SiteID { get; set; }
    }
}
