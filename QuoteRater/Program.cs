﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using QuoteRater.Controllers;
using QuoteRater.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;

namespace QuoteRater
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            //get the command line args
            //const int carrierID = 16;
            int shipmentId = -1;
            int quoteId = 0;
            int scenario = 0;
            int carrierId = 16;
            QuoteRate quoteRate;


            try
            {
                if (args.Length <= 0)
                {
                    throw new Exception("No arguments were supplied!");
                }

                //get shipment ID From arguments
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "-shipmentid")
                    {
                        if (i + 1 < args.Length)
                        {

                            int.TryParse(args[i + 1], out int n);

                            if (Convert.ToBoolean(n))
                            {
                                shipmentId = Convert.ToInt32(args[i + 1]);
                                i++;
                            }
                            else
                            {
                                throw new Exception("Shipment ID is not a number!");
                            }
                        }
                        else
                        {
                            throw new Exception("Shipment ID is not provided!");
                        }
                    }
                    else if (args[i] == "-quoteid")
                    {
                        if (i + 1 < args.Length)
                        {

                            int.TryParse(args[i + 1], out int n);

                            if (Convert.ToBoolean(n))
                            {
                                quoteId = Convert.ToInt32(args[i + 1]);
                                i++;
                            }
                            else
                            {
                                throw new Exception("Quote ID is not a number!");
                            }
                        }
                        else
                        {
                            throw new Exception("Quote ID is not provided!");
                        }
                    }
                    else if (args[i] == "-scenario")
                    {
                        if (i + 1 < args.Length)
                        {

                            int.TryParse(args[i + 1], out int n);

                            if (Convert.ToBoolean(n))
                            {
                                scenario = Convert.ToInt32(args[i + 1]);
                                if (scenario < 0 || scenario > 3)
                                {
                                    throw new Exception("Not a valid scenario!");
                                }

                                i++;
                            }
                            else
                            {
                                throw new Exception("Scenario selected  is not a number!");
                            }
                        }
                        else
                        {
                            throw new Exception("Scenario ID is not provided!");
                        }
                    }
                    else if( args[i] == "-carrier" )
                    {
                        if (i+ 1 < args.Length )
                        {
                            int.TryParse(args[i + 1], out int n);

                            if( Convert.ToBoolean(n))
                            {
                                carrierId = Convert.ToInt32(args[i + 1]);
                                if( carrierId < 0 )
                                {
                                    throw new Exception("CarrierId cannot be negative!");
                                }
                                i++;
                            }
                            else
                            {
                                throw new Exception("Carrier selected is not a number!");
                            }
                        }
                        else
                        {
                            throw new Exception("Carrier ID is not provided!");
                        }
                    }
                }


                //get the information from the database
                IEnumerable<ServiceCallArguments> arguments = QuoteRateMethods.GetArguments(shipmentId, quoteId, carrierId);
                //getShipment Details from Database
                IEnumerable<ShipmentDetails> shipmentDetails = QuoteRateMethods.GetShipmentDetails(shipmentId);
                //get assesorials
                IEnumerable<AccessorialDetails> accessorials = QuoteRateMethods.GetAccessorialDetails(shipmentId, carrierId);



                foreach (ServiceCallArguments argumentSet in arguments)
                {
                    try
                    {

                        quoteRate = CarrierMethods.GetQuoteRate(argumentSet, shipmentDetails, accessorials, carrierId);

                        //Console.WriteLine(quoteRate.JSON);

                        QuoteRateMethods.PutQuote(quoteRate.XML, quoteRate.JSON, quoteRate.surcharge, scenario, argumentSet.QuoteId, carrierId);

                        QuoteRateMethods.UpdateKencoveCost(argumentSet.QuoteId);

                        //Debug - comment out insertQuoteRate, updateKencoveCost above
                        //Console.WriteLine("Carrier Successfully Rated");
                        //Console.WriteLine();
                        //Console.Write( "carrierQuoteID " +quoteRate.carrierQuoteID);
                        //Console.WriteLine();
                        //Thread.Sleep(3000);
                    }
                    catch (Exception ex)
                    {
                        QuoteRateMethods.logError(argumentSet.QuoteId, carrierId, ex.ToString() + " " + ex.Message + " " + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}

