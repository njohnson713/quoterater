﻿using System;
using System.Collections.Generic;
using System.Text;
using QuoteRater.Models;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Xml;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Newtonsoft.Json;
using QuoteRater.YRCAJSONResponse;

namespace QuoteRater.Controllers
{
    public static class QuoteRateMethods
    {
        //Gets the arguments for the call to the YRCA service
        public static IEnumerable<ServiceCallArguments> GetArguments(int shipmentID, int quoteId, int carrierId)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("./appsettings.json");
            IConfiguration configuration = builder.Build();

            IEnumerable<ServiceCallArguments> returnedArgs;


            using (SqlConnection db = new SqlConnection(configuration.GetConnectionString("defaultConnection")))
            {
                try
                {

                    db.Open();
                    var dynamicParameters = new DynamicParameters();

                    dynamicParameters.Add("@shipmentID", shipmentID);
                    dynamicParameters.Add("@quoteID", quoteId);
                    dynamicParameters.Add("@carrierID", carrierId);

                    returnedArgs = db.Query<ServiceCallArguments>("Get_Arguments",
                        dynamicParameters,
                        commandType: CommandType.StoredProcedure
                        );
                    return returnedArgs;
                }
                finally
                {
                    db.Close();
                }

            }
        }

        //Gets the Shipment Details from the shipment id
        public static IEnumerable<ShipmentDetails> GetShipmentDetails(int shipmentID)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("./appsettings.json");
            IConfiguration configuration = builder.Build();

            IEnumerable<ShipmentDetails> returnedDetails;

            using (SqlConnection db = new SqlConnection(configuration.GetConnectionString("defaultConnection")))
            {

                try
                {

                    db.Open();
                    var dynamicParameters = new DynamicParameters();

                    dynamicParameters.Add("@shipmentID", shipmentID);

                    returnedDetails = db.Query<ShipmentDetails>("Get_Shipment_Details",
                        dynamicParameters,
                        commandType: CommandType.StoredProcedure
                        );
                    return returnedDetails;
                }
                finally
                {
                    db.Close();
                }
            }
        }

        //gets the accessorial details from the shipmentId 
        public static IEnumerable<AccessorialDetails> GetAccessorialDetails(int shipmentID, int carrierID)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("./appsettings.json");
            IConfiguration configuration = builder.Build();

            IEnumerable<AccessorialDetails> returnedDetails;
            using (SqlConnection db = new SqlConnection(configuration.GetConnectionString("defaultConnection")))
            {
                try
                {

                    db.Open();
                    var dynamicParameters = new DynamicParameters();

                    dynamicParameters.Add("@shipmentID", shipmentID);
                    dynamicParameters.Add("@carrierID", carrierID);

                    returnedDetails = db.Query<AccessorialDetails>("Get_Accessorial_Details",
                        dynamicParameters,
                        commandType: CommandType.StoredProcedure
                        );
                    return returnedDetails;
                }
                finally
                {
                    db.Close();
                }
            }
        }



        //inputs the XML response into the quote rate table
        public static void PutQuote(string xml, string json, decimal surcharge, int scenario, int quoteId, int carrierId)
        {
            int quoteRateId = 0;
            if (carrierId == 16)
            {
                quoteRateId = InsertQuoteRateYRCA(quoteId, surcharge, json, scenario);
            }
            else if (carrierId == 10)
            {
                quoteRateId = InsertQuoteRatePittOhio(quoteId, surcharge, xml, scenario);
            }
            else if ( carrierId == 3 )
            {
                quoteRateId = InsertQuoteRateADUIEPYLE(quoteId, surcharge, xml, scenario );
            }
        }

        //insert the quoteRateQuote

        /*public static int InsertQuoteRate( int quoteId, decimal surcharge, string json, string xml, int carrierID )
        {
            int quoteRateId = 0;
            if( carrierID == 16 ) {
                    quoteRateId = InsertQuoteRateYRCA( quoteId, surcharge, json );
            }

            return quoteRateId;
        }*/

        public static int InsertQuoteRateYRCA(int quoteId, decimal surcharge, string json, int scenario)
        {

            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("./appsettings.json");
            IConfiguration configuration = builder.Build();
            using (SqlConnection db = new SqlConnection(configuration.GetConnectionString("defaultConnection")))
            {
                int quoteRateId = 0;

                try
                {

                    //deserialize JSON

                    YRCARootJSON response = JsonConvert.DeserializeObject<YRCARootJSON>(json);

                    if (response.isSuccess == true)
                    {

                        decimal totalCost = 0;
                        totalCost = response.pageRoot.bodyMain.rateQuote.ratedCharges.totalCharges / 100;
                        if (totalCost > 0)
                        {
                            totalCost += surcharge;
                        }

                        //Console.WriteLine(response.pageRoot.bodyMain.rateQuote.delivery.standardDays + " Standard Days");
                        //Console.WriteLine(surcharge + " Surcharge " + totalCost + " totalCost" );

                        db.Open();
                        var dynamicParameters = new DynamicParameters();

                        dynamicParameters.Add("@quoteID", quoteId);
                        dynamicParameters.Add("@CarrierID", 16);
                        dynamicParameters.Add("@ShipVia", "YRC");
                        dynamicParameters.Add("@Cost", totalCost);
                        dynamicParameters.Add("@Surcharge", surcharge);
                        dynamicParameters.Add("@TransitTime", response.pageRoot.bodyMain.rateQuote.delivery.standardDays);
                        dynamicParameters.Add("@TerminalPhone", "800-610-6500"); //do we need this?
                        dynamicParameters.Add("@CarrierQuoteID", response.pageRoot.bodyMain.rateQuote.quoteId);
                        dynamicParameters.Add("@json", json);
                        dynamicParameters.Add("@xml", null);
                        dynamicParameters.Add("@scenario", scenario);
                        dynamicParameters.Add("@QuoteRateID", DbType.Int32, direction: ParameterDirection.Output);

                        //deserialize the JSON
                        // Console.WriteLine(json);

                        db.Execute("Put_Quote",
                            dynamicParameters,
                            commandType: CommandType.StoredProcedure
                        );
                    }
                    return quoteRateId;
                }
                finally
                {
                    db.Close();
                }
            }
        }

        public static int InsertQuoteRatePittOhio(int quoteId, decimal surcharge, string xml, int scenario)
        {

            decimal totalCost = 0;
            int transitTimeInt = 0;
            int carrierQuoteIDInt = 0;
            string terminalPhoneString = "";
            int quoteRateId = 0;

            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("./appsettings.json");
            IConfiguration configuration = builder.Build();
            using (SqlConnection db = new SqlConnection(configuration.GetConnectionString("defaultConnection")))
            {
                try
                { 
                    Console.WriteLine(xml);


                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.DtdProcessing = DtdProcessing.Parse;
                    XmlReader xmlReader = XmlReader.Create(xml, settings);

                    //set carrier quote id
                    xmlReader.ReadToFollowing("QuoteID");
                    string carrierQuoteID = xmlReader.ReadElementContentAsString();
                    if (carrierQuoteID.Length > 0)
                    {
                        carrierQuoteIDInt = Convert.ToInt32(carrierQuoteID);
                    }

                    //set cost
                    xmlReader.ReadToFollowing("TotalCharges");
                    string cost = xmlReader.ReadElementContentAsString();
                    if (cost.Length > 0)
                    {
                        totalCost = Convert.ToDecimal(cost) + surcharge;
                    }

                    //set time in transit
                    xmlReader.ReadToFollowing("AdvertisedTransit");
                    string transitTime = xmlReader.ReadElementContentAsString();
                    if (transitTime.Length > 0)
                    {
                        transitTimeInt = Convert.ToInt32(transitTime);
                    }

                    //set terminal phone
                    xmlReader.ReadToFollowing("DestTermPhone");
                    string terminalPhone = xmlReader.ReadElementContentAsString();
                    if (terminalPhone.Length > 0)
                    {
                        terminalPhoneString = terminalPhone;
                    }
                    db.Open();

                    var dynamicParameters = new DynamicParameters();

                    dynamicParameters.Add("@quoteID", quoteId);
                    dynamicParameters.Add("@CarrierID", 10);
                    dynamicParameters.Add("@ShipVia", "PITTOHIO");
                    dynamicParameters.Add("@Cost", totalCost);
                    dynamicParameters.Add("@Surcharge", surcharge);
                    dynamicParameters.Add("@TransitTime", transitTimeInt);
                    dynamicParameters.Add("@TerminalPhone", terminalPhone); //do we need this?
                    dynamicParameters.Add("@CarrierQuoteID", carrierQuoteIDInt);
                    dynamicParameters.Add("@json", null);
                    dynamicParameters.Add("@xml", xml);
                    dynamicParameters.Add("@scenario", scenario);
                    dynamicParameters.Add("@QuoteRateID", DbType.Int32, direction: ParameterDirection.Output);

                    //deserialize the JSON
                    // Console.WriteLine(xml);

                    db.Execute("Put_Quote",
                       dynamicParameters,
                      commandType: CommandType.StoredProcedure
                    );

                }
                catch (Exception exc)
                {
                    logError(quoteId, 10,  exc.ToString() + " | " + exc.Message + " | " + exc.StackTrace);
                }

                return quoteRateId;
            }
        }

        public static int InsertQuoteRateADUIEPYLE(int quoteId, decimal surcharge, string xml, int scenario)
        {
            int transitTimeInt = 0;
            string terminalPhone = "";
            int quoteRateId = 0;

            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("./appsettings.json");
            IConfiguration configuration = builder.Build();
            using (SqlConnection db = new SqlConnection(configuration.GetConnectionString("defaultConnection")))
            {
                try
                {
                    Console.WriteLine(xml);
                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.DtdProcessing = DtdProcessing.Parse;
                    XmlReader xmlReader = XmlReader.Create(xml, settings);
                    //set carrier quote id

                    xmlReader.ReadToFollowing("RateQuoteNumber");
                    string carrierQuoteID = xmlReader.ReadElementContentAsString();

                    xmlReader.ReadToFollowing("shippingDays");
                    int transitTime = xmlReader.ReadElementContentAsInt();

                    xmlReader.ReadToFollowing("totalCharge");
                    string costString = xmlReader.ReadElementContentAsString();
                    decimal totalCost = Convert.ToDecimal(costString);
                    if (totalCost > 0)
                        totalCost += surcharge;

                    db.Open();

                    var dynamicParameters = new DynamicParameters();

                    dynamicParameters.Add("@quoteID", quoteId);
                    dynamicParameters.Add("@CarrierID", 3);
                    dynamicParameters.Add("@ShipVia", "ADUIEPYLE");
                    dynamicParameters.Add("@Cost", totalCost);
                    dynamicParameters.Add("@Surcharge", surcharge);
                    dynamicParameters.Add("@TransitTime", transitTimeInt);
                    dynamicParameters.Add("@TerminalPhone", terminalPhone); 
                    dynamicParameters.Add("@CarrierQuoteID", Convert.ToInt32(carrierQuoteID));
                    dynamicParameters.Add("@json", null);
                    dynamicParameters.Add("@xml", xml);
                    dynamicParameters.Add("@scenario", scenario);
                    dynamicParameters.Add("@QuoteRateID", DbType.Int32, direction: ParameterDirection.Output);

                    //deserialize the JSON
                    // Console.WriteLine(xml);

                    db.Execute("Put_Quote",
                       dynamicParameters,
                      commandType: CommandType.StoredProcedure
                    );

                }
                catch (Exception exc)
                {
                    logError(quoteId, 3, exc.ToString() + " | " + exc.Message + " | " + exc.StackTrace);
                }

                return quoteRateId;
            }
        }

        //update Kencove Cost for rate in QuoteRate table
        public static void UpdateKencoveCost(int quoteID)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("./appsettings.json");
            IConfiguration configuration = builder.Build();
            using (SqlConnection db = new SqlConnection(configuration.GetConnectionString("defaultConnection")))
            {
                try
                {
                    db.Open();
                    var dynamicParameters = new DynamicParameters();
                    dynamicParameters.Add("@quoteID", quoteID);
                    db.Execute("Ship_RateLTL_SetCustCost",
                        dynamicParameters,
                        commandType: CommandType.StoredProcedure
                        );
                }
                catch( Exception exc )
                {
                    Console.WriteLine(exc);
                }
                finally
                {
                    db.Close();
                }
            }

        }
        public static void logError(int quoteID, int carrierID, string error)
        {
            error = error.Replace("'", "''");
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("./appsettings.json");
            IConfiguration configuration = builder.Build();
            using (SqlConnection db = new SqlConnection(configuration.GetConnectionString("defaultConnection")))
            {
                try
                {
                    db.Open();
                    var dynamicParameters = new DynamicParameters();
                    dynamicParameters.Add("@quoteID", quoteID);
                    dynamicParameters.Add("@CarrierID", carrierID);
                    dynamicParameters.Add("@Error", error);
                    db.Execute("Ship_QuoteRateError_Insert",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure
                    );
                }
                finally
                {
                    db.Close();
                }
            }
        }
    }
}


