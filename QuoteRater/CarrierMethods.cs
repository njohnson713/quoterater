﻿using System;
using System.Collections.Generic;
using System.Text;
using QuoteRater.Models;
using System.IO;
using System.Xml;
using System.Web;
using System.Net;

namespace QuoteRater
{
    public static class CarrierMethods
    {

        public static QuoteRate GetQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials, int carrierId)
        {
            QuoteRate quoteRate = new QuoteRate();

            if (carrierId == 16)
            {
                quoteRate = GetYRCAQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 10)
            {
                quoteRate = GetPittOhioQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 11)
            {
                quoteRate = GetVitranQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 13)
            {
                quoteRate = GetUPSFQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 14)
            {
                quoteRate = GetUSPSQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 15)
            {
                quoteRate = GetWARDQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 17)
            {
                quoteRate = GetOldDominionQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 19)
            {
                quoteRate = GetCTIQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 3)
            {
                quoteRate = GetADUIEPYLEQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 4)
            {
                quoteRate = GetCCXQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 6)
            {
                quoteRate = GetEstesQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 7)
            {
                quoteRate = GetFedExQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 8)
            {
                quoteRate = GetNEMFQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 9)
            {
                quoteRate = GetNewPennQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            else if (carrierId == 25)
            {
                quoteRate = GetSaiaQuoteRate(argumentSet, shipmentDetails, accessorials);
            }
            return quoteRate;
        }

        private static QuoteRate GetPittOhioQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();

            try
            {
                //run PittOhio rate
                string requestURL = "http://works.pittohio.com/mypittohio/b2bratecalc.asp";

                string data = "acctnum=" + argumentSet.UserID +
                                "&password=" + argumentSet.Password +
                                "&shipcity=" + argumentSet.OriginCity +
                                "&shipstate=" + argumentSet.OriginState +
                                "&shipzip=" + argumentSet.OriginZip +
                                "&conscity=" + argumentSet.DestCity +
                                "&consstate=" + argumentSet.DestState +
                                "&conszip=" + argumentSet.DestZip;

                //Items
                int detailCount = 1;
                foreach (ShipmentDetails detail in shipmentDetails)
                {
                    data += "&class" + detailCount + "=" + detail.Class;
                    data += "&weight" + detailCount + "=" + CalculateShipWeight(detail.Weight, detail.PackagingWeightPct, detail.PackagingWeight, decimal.Parse(detail.Pieces), detail.TrueLTLRate ? 1 : 0).ToString();

                    detailCount++;
                }


                //Accessorials
                foreach (AccessorialDetails access in accessorials)
                {
                    //Over Length - 15FT Surcharge
                    if (access.AccessorialID == 7)
                    {
                        quoteRate.surcharge += access.Charge;
                    }

                    //Residential Delivery
                    if (access.AccessorialID == 3)
                    {
                        data += "&acc_rdel=Y";
                    }

                    //Notification
                    if (access.AccessorialID == 2)
                    {
                        data += "&acc_mnc=Y";
                    }
                }

                //run request, get response, load to xml reader
                requestURL = requestURL + "?" + data;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestURL);
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                // Debugging - Use to look at response
                // Stream responseStream = response.GetResponseStream();
                // StreamReader debugData = new StreamReader(responseStream);
                //  string result = debugData.ReadToEnd();


                Stream responseStream = response.GetResponseStream();
                StreamReader streamData = new StreamReader(responseStream);
                string result = streamData.ReadToEnd();

                //fill QuoteRate with rate response information
                quoteRate.QuoteID = argumentSet.QuoteId;
                quoteRate.CarrierID = 10;
                quoteRate.XML = result;
                quoteRate.JSON = null;
                //quoteRate.shipVia = "PITTOHIO";
            } catch (Exception ex )
            {
                throw ex;
            }

            return quoteRate;
        }

        private static QuoteRate GetSaiaQuoteRate( ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails>  shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            string AccountNumber;

            
                
                if (argumentSet.SiteID == 3)
                {
                     AccountNumber = "1028905";
                }
                else if (argumentSet.SiteID == 4)
                {
                    AccountNumber = "0976308";
                }
                else if (argumentSet.SiteID == 11)
                {
                    AccountNumber = "0976309";
                }
                else if (argumentSet.SiteID == 13)
                {
                    AccountNumber = "0976310";
                }
                else if (argumentSet.SiteID == 16)
                {
                    AccountNumber = "0789862";
                }
                else
                {
                    AccountNumber = "1028905";
                }
            
                /*

                    //RateQuote ratequote = new RateQuote();
                    Create request = new Create();

                    // --------Account info //-- not current set up with saia
                    request. = "Kencove";
                    request.Password = "Alexandra1";
                    request.TestMode = "Y";
                    request.BillingTerms = "Prepaid";
                    request.AccountNumber = getAccountNum(quote.siteID);//"1028905";
                                                                        //Console.WriteLine(quote.siteID);
                                                                        // --------destination
                    request.Application = "Outbound";
                    request.DestinationCity = shipment.city;
                    request.DestinationState = shipment.state;
                    request.DestinationZipcode = shipment.zip.Substring(0, 5);
                    List<DetailItem> details = new List<DetailItem>();
                    List<AccessorialItem> accList = new List<AccessorialItem>();

                    foreach (ShipmentAccessorial sa in shipment.accessorials)
                    {
                        // Console.WriteLine(sa.accessorialToCarrier.accessorialCode);
                        AccessorialItem acc = new AccessorialItem();
                        acc.Code = sa.accessorialToCarrier.accessorialCode;
                        accList.Add(acc);
                    }
                    request.Accessorials = accList.ToArray();

                    foreach (ShipmentDetail shipDetail in shipmentDetails)
                    {
                        DetailItem d = new DetailItem();
                        d.Class = shipDetail.truckClass;
                        d.Weight = Convert.ToInt32(shipDetail.weight);
                        details.Add(d);

                    }

                    request.Details = details.ToArray();

                    try
                    {
                        Response response = ratequote.Create(request);
                        if (response.Code != "")
                        {
                            // Add error handling code in case Saia responds 
                            // with an Error Code

                            //this.ResponseCode.Text = response.Code;
                            //this.Element.Text = response.Element;
                            //this.Fault.Text = response.Fault;
                            //this.Message.Text = response.Message;
                        }
                        else
                        {
                            //Console.WriteLine(response.QuoteNumber);
                            //Console.WriteLine(response.TotalInvoice);

                            quoteRate.quoteID = quote.quoteID;
                            quoteRate.carrierID = carrierID;
                            quoteRate.shipVia = "SAIA";
                            quoteRate.transitTime = Convert.ToInt32(response.StandardServiceDays);
                            quoteRate.cost = response.TotalInvoice;
                            quoteRate.carrierQuoteID = Convert.ToString(response.QuoteNumber);
                            quoteRate.terminalPhone = "";
                            quoteRate.surcharge = surcharge;

                            //log error or insert rate
                            if (response.Code != "")
                            {
                                string error = response.Code;
                                if (error.Length > 0)
                                {
                                    throw new Exception(error);
                                }
                            }
                            else
                            {
                                insertQuoteRate();
                                updateKencoveCost(quote.quoteID);

                                //Debug - comment out insertQuoteRate, updateKencoveCost above
                                //Console.WriteLine("Old Dominion Successfully Rated");
                                //Console.WriteLine("$" + quoteRate.cost.ToString());
                                //Console.ReadLine();
                            }
                            //this.TotalInvoice.Text = response.TotalInvoice.ToString();
                            //this.OriginManager.Text = response.OriginTerminal.Manager;

                            //// Below is a sample of how to retrieve an array
                            //// using SOAP objects.

                            //foreach (Saia.RateQuote.RateDetailItem RateDetailItem in response.RateDetails)
                            //{
                            //    Rate1.Text = RateDetailItem.Rate.ToString();
                            //    break;
                            //}
                        }

                    }
                    catch
                    {
                        // Add error handling code in case your unable to connect 
                        // to the Saia Website.
                    }



                }

            }
            catch (Exception ex)
            {
                //logError(quote.quoteID, carrierID, ex.ToString());

                //Debug - comment out logError above
                //Console.WriteLine(ex.ToString());
                //Console.ReadLine();
            }
     
            
   
            */
            return quoteRate;
        }

        private static QuoteRate GetVitranQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }

        private static QuoteRate GetUPSFQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetUSPSQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetWARDQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetOldDominionQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetCTIQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }

        private static QuoteRate GetADUIEPYLEQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();

            decimal surcharge = 0;

            string requestURL = "http://www.aduiepyle.com/publicdocs/LTLRateCalc3_XML.aspx";
            string postData = "MyPyleID=" + argumentSet.UserID
                + "&account=" + HttpUtility.UrlEncode(argumentSet.AccountNo)
                + "&inboundOutbound=O"
                + "&oZip=" + HttpUtility.UrlEncode(argumentSet.OriginZip.Substring(0, 5))
                + "&dZip=" + HttpUtility.UrlEncode(argumentSet.DestZip.Substring(0, 5));

            //shipment details with data
            string weights = "";
            string classes = "";
            string pieces = "";
            string containerTypes = "";
            string isFull = "";
            string dimensions = "";

            foreach (ShipmentDetails shipDetail in shipmentDetails)
            {
                weights += "," + CarrierMethods.CalculateShipWeight(shipDetail.Weight, shipDetail.PackagingWeightPct, shipDetail.PackagingWeight, Convert.ToDecimal(shipDetail.Pieces), Convert.ToInt32(shipDetail.TrueLTLRate)).ToString();
                classes += "," + shipDetail.Class;
                pieces += "," + shipDetail.Pieces.ToString();
                containerTypes += ",PALLET";
                isFull += ",FULL";
                dimensions += ",48x40x48";

            }

            weights = weights.TrimStart(',');
            classes = classes.TrimStart(',');
            pieces = pieces.TrimStart(',');
            containerTypes = containerTypes.TrimStart(',');
            isFull = isFull.TrimStart(',');
            dimensions = dimensions.TrimStart(',');

            postData += "&weights=" + weights;
            postData += "&classes=" + classes;
            postData += "&count=" + pieces;
            postData += "&CtrType=" + containerTypes;
            postData += "&isFull=" + isFull;
            postData += "&dim=" + dimensions;

            //accessorials
            foreach (AccessorialDetails shipAccess in accessorials)
            {
                surcharge += shipAccess.Charge;
                switch (shipAccess.AccessorialID)
                {
                    case 1: //COD
                        break;
                    case 2: //Notification
                        postData += "&ncall=1";
                        break;
                    case 3: //Residential Delivery
                        postData += "&nresid=1";
                        break;
                    case 4: //Residential Pickup
                        break;
                    case 5: //Single Shipment
                        postData += "&sship=1";
                        break;
                }
            }

            //run request, get response, load to xml reader
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestURL + "?" + postData);
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            StreamReader streamData = new StreamReader(responseStream);
            string result = streamData.ReadToEnd();


            quoteRate.surcharge = surcharge;
            quoteRate.JSON = null;
            quoteRate.XML = result;
            quoteRate.QuoteID = argumentSet.QuoteId;
            quoteRate.CarrierID = 3;

            // Debugging - Use to look at response
            //  Stream responseStream = response.GetResponseStream();
            //  StreamReader data = new StreamReader(responseStream);
            //  string result = data.ReadToEnd();
            return quoteRate;
        }
        private static QuoteRate GetCCXQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetEstesQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetFedExQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetNEMFQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetNewPennQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            return quoteRate;
        }
        private static QuoteRate GetYRCQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {


            QuoteRate quoteRate = new QuoteRate();
            string url;
            string destZipSub;
            string originZipSub;

            if (argumentSet.DestZip != null)
            {
                destZipSub = argumentSet.DestZip.Substring(0, 5);
            }
            else
            {
                destZipSub = "";
            }

            if (argumentSet.OriginZip != null)
            {
                originZipSub = argumentSet.OriginZip.Substring(0, 5);
            }
            else
            {
                originZipSub = "";
            }


            url = "https://my.yrc.com/dynamic/national/servlet?" +
                                    "CONTROLLER=com.rdwy.ec.rexcommon.proxy.http.controller.ProxyApiController" +
                                    "&redir=/tfq561" +
                                    "&LOGIN_USERID=" + HttpUtility.UrlEncode(argumentSet.UserID) +
                                    "&LOGIN_PASSWORD=" + HttpUtility.UrlEncode(argumentSet.Password) +
                                    "&BusId=" + HttpUtility.UrlEncode(argumentSet.AccountNo) +
                                    "&BusRole=Shipper" +
                                    "&OrigCityName=" + HttpUtility.UrlEncode(argumentSet.OriginCity) +
                                    "&OrigStateCode=" + HttpUtility.UrlEncode(argumentSet.OriginState) +
                                    "&OrigZipCode=" + HttpUtility.UrlEncode(originZipSub) +
                                    "&OrigNationCode=USA" +
                                    "&DestCityName=" + HttpUtility.UrlEncode(argumentSet.DestCity) +
                                    "&DestStateCode=" + HttpUtility.UrlEncode(argumentSet.DestState) +
                                    "&DestZipCode=" + HttpUtility.UrlEncode(destZipSub) +
                                    "&ServiceClass=STD" +
                                    "&PickupDate=" + HttpUtility.UrlEncode(String.Format("{0:yyyyMMdd}", DateTime.Now)) +
                                    "&TypeQuery=QUOTE";

            //shipment Country
            string shipToCountry = argumentSet.Country.Substring(0, 2) == "CA" ? "CAN" : "USA";
            url += "&DestNationCode=" + HttpUtility.UrlEncode(shipToCountry);

            //terms
            url += "&PaymentTerms=PrePaid";

            //line items
            int shipDetailCount = 1;
            foreach (ShipmentDetails shipDetail in shipmentDetails)
            {
                url += "&LineItemNmfcClass" + shipDetailCount + "=" + HttpUtility.UrlEncode(shipDetail.Class);
                url += "&LineItemWeight" + shipDetailCount + "=" + HttpUtility.UrlEncode(CalculateShipWeight(shipDetail.Weight, shipDetail.PackagingWeightPct, shipDetail.PackagingWeight, decimal.Parse(shipDetail.Pieces), shipDetail.TrueLTLRate ? 1 : 0).ToString());

                shipDetailCount++;
            }
            url += "&LineItemCount=" + HttpUtility.UrlEncode(Convert.ToString(shipDetailCount - 1));

            //accessorials
            int accessorialCount = 1;
            foreach (AccessorialDetails access in accessorials)
            {
                url += "&AccOption" + accessorialCount + "=" + HttpUtility.UrlEncode(access.AccessorialCode);
                url += "&AccOptionCount=1";
            }

            //run request, get response, load to xml reader
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //Console.Write(response.StatusCode);
            //Console.Write(response.Headers);
            //Console.Write(response.ToString());


            // Debugging - Use to look at response
            Stream responseStream = response.GetResponseStream();
            StreamReader data = new StreamReader(responseStream);
            string result = data.ReadToEnd();


            quoteRate.CarrierID = 16;
            quoteRate.QuoteID = argumentSet.QuoteId;
            quoteRate.XML = result;
            quoteRate.JSON = null;

            //Console.Write(result);

            /*XmlTextReader xmlReader = new XmlTextReader(response.GetResponseStream());

            xmlReader.ReadToFollowing("ReferenceId");
            string carrierQuoteID = xmlReader.ReadElementContentAsString();

            xmlReader.ReadToFollowing("StandardDate");
            int intDeliveryDate = xmlReader.ReadElementContentAsInt();
            DateTime deliveryDate = DateTime.ParseExact(intDeliveryDate.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);

            xmlReader.ReadToFollowing("StandardDays");
            int transitTime = xmlReader.ReadElementContentAsInt();

            xmlReader.ReadToFollowing("TotalCharges");
            string costString = xmlReader.ReadElementContentAsString();

            decimal cost = Convert.ToDecimal(costString.Insert(costString.Length - 2, "."));
            if (cost > 0)
                cost += surcharge;

            quoteRate = new QuoteRate();


            //fill QuoteRate with rate response information
            quoteRate.QuoteID = argumentSet.QuoteId;
            quoteRate.CarrierID = 16;
            quoteRate.ShipVia = "YRC";
            quoteRate.Cost = cost;
            quoteRate.TransitTime = transitTime;
            quoteRate.CarrierQuoteID = carrierQuoteID;
            quoteRate.TerminalPhone = "";
            //quoteRate.Surcharge = surcharge;
            //quoteRate.deliveryDate = deliveryDate;
            */



           return quoteRate;
        }


        private static QuoteRate GetYRCAQuoteRate(ServiceCallArguments argumentSet, IEnumerable<ShipmentDetails> shipmentDetails, IEnumerable<AccessorialDetails> accessorials)
        {
            QuoteRate quoteRate = new QuoteRate();
            string jsonData = "";
            string today = DateTime.Today.ToString("yyyyMMdd");//Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();

            string destZipSub;
            string originZipSub;
            string destCountry;

            try
            {
                if (argumentSet.DestZip != null)
                {
                    destZipSub = argumentSet.DestZip.Substring(0, 5);
                }
                else
                {
                    destZipSub = "";
                }

                if (argumentSet.OriginZip != null)
                {
                    originZipSub = argumentSet.OriginZip.Substring(0, 5);
                }
                else
                {
                    originZipSub = "";
                }

                if (argumentSet.DestCountry == null || argumentSet.DestCountry == "")
                {
                    destCountry = "USA";
                }
                else
                {
                    destCountry = argumentSet.DestCountry;
                }

                WebRequest request = WebRequest.Create("https://api.yrc.com/node/api/ratequote");

                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                string postData =
                    "{\"login\":{\"username\":\"" + argumentSet.UserID +
                    "\",\"password\":\"" + argumentSet.Password +
                    "\",\"busId\":\"" + argumentSet.AccountNo +
                    "\",\"busRole\":\"Shipper" +
                    "\",\"paymentTerms\":\"Prepaid\"},\"details\":{\"serviceClass\":\"ACEL\",\"typeQuery\":\"QUOTE" +
                    "\",\"pickupDate\":\"" + today +
                    "\"},\"originLocation\":{\"city\":\"" + argumentSet.OriginCity +
                    "\",\"state\":\"" + argumentSet.OriginState +
                    "\",\"postalCode\":\"" + originZipSub +
                    "\",\"country\":\"" + "USA" +
                    "\"},\"destinationLocation\":{\"city\":\"" + argumentSet.DestCity +
                    "\",\"state\":\"" + argumentSet.DestState +
                    "\",\"postalCode\":\"" + destZipSub +
                    "\",\"country\":\"" + destCountry +
                    "\"},\"listOfCommodities\":{\"commodity\":[";

                quoteRate.surcharge = 0;

                foreach (ShipmentDetails details in shipmentDetails)
                {
                    quoteRate.surcharge += details.Surchage;

                    postData += "{\"nmfcClass\":\"" + details.Class +
                    "\",\"packageLength\":48" +
                    ",\"packageWidth\":48" +
                    ",\"packageHeight\":48" +
                    ",\"weight\":" + CalculateShipWeight(details.Weight, details.PackagingWeightPct, details.PackagingWeight, decimal.Parse(details.Pieces), details.TrueLTLRate ? 1 : 0).ToString() +
                    "},";
                }
                postData = postData.TrimEnd(',');
                postData += "]},\"serviceOpts\":{\"accOptions\":[";
                foreach (AccessorialDetails details in accessorials)
                {
                    postData += "\"" + details.AccessorialCode + "\",";
                }
                postData = postData.TrimEnd(',');

                postData += "]}}";

                Console.Write(postData);

                //postData = "{\"login\": {\"username\":\"rapi0030\",\"password\":\"2063224\",\"busId\":\"80500977968\",\"busRole\":\"Shipper\",\"paymentTerms\":\"Prepaid\"},\"details\":{\"serviceClass\":\"STD\",\"typeQuery\":\"QUOTE\",\"pickupDate\":\"20170620\"},\"originLocation\":{\"city\":\"Overland Park\",\"state\":\"KS\",\"postalCode\":\"66211\",\"country\":\"USA\"},\"destinationLocation\":{\"city\":\"Akron\",\"state\":\"OH\",\"postalCode\":\"44310\",\"country\":\"USA\"},\"listOfCommodities\":{\"commodity\":[{\"nmfcClass\":50,\"weight\":500}]},\"serviceOpts\":{\"accOptions\":[\"NTFY\"]}}";

                //Console.Write(postData);

                //get a reference to the request-stream, and write the postData to it
                using (Stream s = request.GetRequestStream())
                {
                    using (StreamWriter sw = new StreamWriter(s))
                        sw.Write(postData);
                }

                //get response-stream, and use a streamReader to read the content
                using (Stream s = request.GetResponse().GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(s))
                    {
                        jsonData = sr.ReadToEnd();
                    }
                }

                //Console.Write(jsonData);

                quoteRate.JSON = jsonData;
                quoteRate.QuoteID = argumentSet.QuoteId;
                quoteRate.XML = null;
                quoteRate.CarrierID = 16;
            } catch( Exception ex )
            {
                throw ex;
            }
            return quoteRate;
        }

        //adds parcel weights to shipDetail weights
        private static int CalculateShipWeight(decimal weight, decimal packagingWeightPct, decimal packagingWeight, decimal pieceCount, int trueLTLRate = 0)
        {
            if (trueLTLRate != 1)
            {
                //weight = packaging weight pct + (totalitemweight * pieceCount)
                weight = (weight * packagingWeightPct) + (packagingWeight * pieceCount);
                return Convert.ToInt32(weight);
            }
            else
            {
                return Convert.ToInt32(weight);
            }
        }
    }
}



