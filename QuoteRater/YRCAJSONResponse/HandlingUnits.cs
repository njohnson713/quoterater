﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class HandlingUnits
    {
        public int count { get; set; }
        public int length { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }
}

