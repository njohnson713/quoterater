﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class RateQuote
    {
        public string referenceId { get; set; }
        public string quoteId { get; set; }
        public string paymentTerms { get; set; }
        public string payerRole { get; set; }
        public int pickupDate { get; set; }
        public Delivery delivery { get; set; }
        public string currency { get; set; }
        public List<Location> location { get; set; }
        public List<Customer> customer { get; set; }
        public List<LineItem> lineItem { get; set; }
        public string vendorSCAC { get; set; }
        public string publishRateFlag { get; set; }
        public string minCharge { get; set; }
        public string guaranteeFlag { get; set; }
        public RatedCharges ratedCharges { get; set; }
        public string shipmentRateUnit { get; set; }
        public int shipmentPricePerUnit { get; set; }
        public List<Pricing> pricing { get; set; }
        public PromoCodeInfo promoCodeInfo { get; set; }
        public Liability liability { get; set; }
        public string dimFactor { get; set; }
        public string ratedPricingDays { get; set; }
        public string accSrvcCount { get; set; }
        public string commodityCount { get; set; }
        public string minChargeFloor { get; set; }
        public int weight { get; set; }
        public int ratedWeight { get; set; }
        public string quoteType { get; set; }
        public string linearFeet { get; set; }
        public string cubicFeet { get; set; }
        public string calcCubicFeet { get; set; }
        public string fullVisibleCapacity { get; set; }
        public string positionHours { get; set; }
    }
}
