﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class Delivery
    {
        public RequestedServiceType requestedServiceType { get; set; }
        public RequestedDateTime requestedDateTime { get; set; }
        public int standardDate { get; set; }
        public int standardDays { get; set; }
        public int deliveryDate { get; set; }
    }
}
