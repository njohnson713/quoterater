﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class RatedCharges
    {
        public int freightCharges { get; set; }
        public int otherCharges { get; set; }
        public int totalCharges { get; set; }
    }
}

