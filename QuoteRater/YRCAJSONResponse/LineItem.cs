﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class LineItem
    {
        public string type { get; set; }
        public HandlingUnits handlingUnits { get; set; }
        public Pieces pieces { get; set; }
        public string hazardous { get; set; }
        public Nmfc nmfc { get; set; }
        public int weight { get; set; }
        public string density { get; set; }
        public string ratedType { get; set; }
        public string ratedClass { get; set; }
        public int ratedWeight { get; set; }
        public string rate { get; set; }
        public string rateUOM { get; set; }
        public string charges { get; set; }
        public string weightUOM { get; set; }
        public string ratePerUOM { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public string terms { get; set; }
        public string creditInd { get; set; }
        public string disposition { get; set; }
        public string sourceCode { get; set; }
        public string quantity { get; set; }
    }
}
