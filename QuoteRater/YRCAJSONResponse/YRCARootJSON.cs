﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class YRCARootJSON
    {
        public Boolean isSuccess { get; set; }
        public List<String> errors { get; set; }
        public object warnings { get; set; }
        public PageRoot pageRoot { get; set; }
    }
}
