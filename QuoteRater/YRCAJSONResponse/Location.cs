﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class Location
    {
        public string role { get; set; }
        public string city { get; set; }
        public string statePostalCode { get; set; }
        public string zipCode { get; set; }
        public string nationCode { get; set; }
        public int terminal { get; set; }
        public string zone { get; set; }
        public string typeService { get; set; }
        public string airportCode { get; set; }
    }
}
