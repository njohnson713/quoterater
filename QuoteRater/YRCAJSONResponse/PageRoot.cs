﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class PageRoot
    {
        public String context { get; set; }
        public String programID { get; set; }
        public String queryType { get; set; }
        public String dateTime { get; set; }
        public String returnCode { get; set; }
        public String returnText { get; set; }
        public String recordCount { get; set; }
        public String recordOffset { get; set; }
        public String maxRecords { get; set; }
        public PageHead pageHead { get; set; }
        public BodyHead bodyHead { get; set; }
        public BodyMain bodyMain { get; set; }

    }
}
