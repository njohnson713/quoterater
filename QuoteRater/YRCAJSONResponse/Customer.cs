﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class Customer
    {
        public string role { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string statePostalCode { get; set; }
        public string zipCode { get; set; }
        public string nationCode { get; set; }
        public string terminal { get; set; }
        public string account { get; set; }
        public string busId { get; set; }
        public string locationCreditIndicator { get; set; }
    }
}
