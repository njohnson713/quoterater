﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class Pricing
    {
        public string agent { get; set; }
        public string tariff { get; set; }
        public string item { get; set; }
        public string applyPayTerms { get; set; }
        public string usedFlag { get; set; }
        public string role { get; set; }
        public string pricingType { get; set; }
        public string scheduleType { get; set; }
        public string type { get; set; }
    }
}

