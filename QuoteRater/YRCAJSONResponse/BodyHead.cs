﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class BodyHead
    {
        public String bodyTitle { get; set; }
        public String pageSubtitle { get; set; }
    }
}

