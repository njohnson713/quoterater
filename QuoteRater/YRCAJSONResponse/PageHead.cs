﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater.YRCAJSONResponse
{
    public class PageHead
    {
        public String pageTitle { get; set; }
        public String pageSubtitle { get; set; }
    }
}
