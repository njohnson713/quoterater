﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteRater
{
    public class QuoteRate
    {
        public int carrierQuoteID { get; set; }
        public int QuoteID { get; set; }
        public int CarrierID { get; set; }
        public string XML { get; set; }
        public string JSON { get; set; }
        public decimal surcharge { get; set; }
        public QuoteRate() { }

    }
}
